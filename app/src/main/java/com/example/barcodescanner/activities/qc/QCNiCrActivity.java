package com.example.barcodescanner.activities.qc;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.barcodescanner.R;
import com.example.barcodescanner.preferences.AppPreferences;
import com.example.barcodescanner.tools.BarcodeHandler;
import com.example.barcodescanner.tools.SidebarHandler;
import com.example.barcodescanner.tools.UtilsUI;

import java.util.List;

public class QCNiCrActivity extends AppCompatActivity {
    public TextView barcodeResultTextView;
    public TextView serverResponseTextView;
    public TableLayout tableLayout;
    public AppPreferences appPreferences;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private EditText barcodeEditText;
    private EditText barcodeEditTextQuantity;
    private EditText barcodeEditTextBoxType;
    public SidebarHandler sidebarHandler;
    public final String currentLine = "Kontrola jakości (NiCr)";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qc_ni_cr);

        // Barcode input field
        barcodeEditTextQuantity = findViewById(R.id.barcodeEditTextQuantity);
        barcodeEditTextBoxType = findViewById(R.id.barcodeEditTextBoxType);
        barcodeEditText = findViewById(R.id.barcodeEditText);

        barcodeEditTextQuantity.setInputType(InputType.TYPE_NULL);
        barcodeEditTextBoxType.setInputType(InputType.TYPE_NULL);
        barcodeEditText.setInputType(InputType.TYPE_NULL);

        // Barcode responses
        barcodeResultTextView = findViewById(R.id.barcodeResultTextView);
        serverResponseTextView = findViewById(R.id.serverResponseTextView);

        appPreferences = new AppPreferences(this);

        tableLayout = findViewById(R.id.tableLayout);

        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);

        sidebarHandler = new SidebarHandler(this, tableLayout, appPreferences, currentLine, barcodeResultTextView, serverResponseTextView);

        // Auto Send
        CheckBox checkBoxVariable = findViewById(R.id.checkBoxVariable);
        checkBoxVariable.setChecked(appPreferences.getAutoSendState());
        checkBoxVariable.setOnCheckedChangeListener((buttonView, isChecked) -> {
            // Update the variable state
            appPreferences.setAutoSendState(isChecked);
        });

        Button retrieveValuesButton = findViewById(R.id.retrieveValuesButton);

        // Set an OnClickListener for the button
        retrieveValuesButton.setOnClickListener(v -> {
            // Retrieve stored values and display them in an alert or dialog
            List<String> storedValues = appPreferences.getUnsentCodes();
            sidebarHandler.showAlertDialog(storedValues);
        });

        Button clearValuesButton = findViewById(R.id.clearValuesButton);
        // Set an OnClickListener for the clear button
        clearValuesButton.setOnClickListener(v -> {
            // Retrieve stored values and display them in an alert or dialog
            sidebarHandler.showClearAlertDialog();
        });

        // On Edit (build-in scanners types down code and presses enter
        barcodeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // This method is called to notify you that somewhere within s, the text is about to be replaced.
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // This method is called to notify you that somewhere within s, the text has been replaced.
            }

            @Override
            public void afterTextChanged(Editable editable) {
                // This method is called to notify you that the characters within s are about to be replaced with new text.
            }
        });

        barcodeEditText.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            int scanCode;
            if (appPreferences.getAutoSendState()) {
                scanCode = 0;
            } else {
                scanCode = 17;
            }
            if (actionId == EditorInfo.IME_ACTION_DONE ||
                    (keyEvent != null && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN && keyEvent.getScanCode() == scanCode)) {
                // Enter key pressed or action button pressed
                String scannedCodeQuantity = barcodeEditTextQuantity.getText().toString();
                String scannedCodeBoxType = barcodeEditTextBoxType .getText().toString();
                String scannedCode = barcodeEditText.getText().toString();
                String scannedCode2 = scannedCodeBoxType + " " + scannedCodeQuantity;

                // Process the scanned code, e.g., send it to your server
                sendBarcodeRequest(scannedCode, scannedCode2);

                // Clear the EditText for the next scan
                barcodeEditTextQuantity.setText("");
                barcodeEditTextBoxType.setText("");
                barcodeEditText.setText("");

                // Use View.post to ensure that the focus is set after the view is fully initialized
                runOnUiThread(() -> barcodeEditTextQuantity.post(barcodeEditText::requestFocus));
                return true;
            }
            return false;
        });

        barcodeEditText.setOnFocusChangeListener((v, hasFocus) -> UtilsUI.setEditTextBackground(hasFocus, barcodeEditText, this));
        barcodeEditTextQuantity.setOnFocusChangeListener((v, hasFocus) -> UtilsUI.setEditTextBackground(hasFocus, barcodeEditTextQuantity, this));
        barcodeEditTextBoxType.setOnFocusChangeListener((v, hasFocus) -> UtilsUI.setEditTextBackground(hasFocus, barcodeEditTextBoxType, this));

        // Initial focus
        runOnUiThread(() -> barcodeEditTextQuantity.post(barcodeEditTextQuantity::requestFocus));

        // Table displaying successful scans
        List<List<String>> allLists = appPreferences.getWHlistOther();

        UtilsUI.appendTable(allLists, tableLayout, layoutParams, this);
    }

    private void sendBarcodeRequest(String barcodeData, String barcodeData2) {
        // Pass the callback to the BarcodeRequestTask
        BarcodeHandler barcodeHandler = new BarcodeHandler(this, tableLayout, barcodeData, barcodeData2,"", currentLine, barcodeResultTextView, serverResponseTextView);
        barcodeHandler.sendBarcodeRequest();
    }
    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        return drawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }
}