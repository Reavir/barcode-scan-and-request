package com.example.barcodescanner.activities.qc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.example.barcodescanner.R;
import com.example.barcodescanner.activities.warehouse.WHActivity;
import com.example.barcodescanner.activities.warehouse.WH_ExportActivity;
import com.example.barcodescanner.activities.warehouse.WH_ImportActivity;

public class QCActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qc);

        Button KTLButton = findViewById(R.id.KTLButton);
        Button NiCrButton = findViewById(R.id.NiCrButton);

        KTLButton.setOnClickListener(v -> {
            Intent intent = new Intent(QCActivity.this, QCKTLActivity.class);
            startActivity(intent);
        });

        NiCrButton.setOnClickListener(v -> {
            Intent intent = new Intent(QCActivity.this, QCNiCrActivity.class);
            startActivity(intent);
        });
    }
}