package com.example.barcodescanner.activities.warehouse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.example.barcodescanner.R;

public class WHActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wh);

        Button ImportButton = findViewById(R.id.Import);
        Button ExportButton = findViewById(R.id.Export);

        ImportButton.setOnClickListener(v -> {
            Intent intent = new Intent(WHActivity.this, WH_ImportActivity.class);
            startActivity(intent);
        });

        ExportButton.setOnClickListener(v -> {
            Intent intent = new Intent(WHActivity.this, WH_ExportActivity.class);
            startActivity(intent);
        });
    }
}