package com.example.barcodescanner.activities.warehouse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.example.barcodescanner.R;

public class WH_ExportActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wh_export);

        Button ExportBrussButton = findViewById(R.id.ExportBruss);
        Button ExportMubeaButton = findViewById(R.id.ExportMubea);

        ExportBrussButton.setOnClickListener(v -> {
            Intent intent = new Intent(WH_ExportActivity.this, WH_Export_Bruss_Activity.class);
            startActivity(intent);
        });

        ExportMubeaButton.setOnClickListener(v -> {
            Intent intent = new Intent(WH_ExportActivity.this, WH_Export_Other_Activity.class);
            startActivity(intent);
        });
    }

}