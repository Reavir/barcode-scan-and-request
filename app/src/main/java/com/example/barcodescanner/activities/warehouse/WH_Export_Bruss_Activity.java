package com.example.barcodescanner.activities.warehouse;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.barcodescanner.R;
import com.example.barcodescanner.preferences.AppPreferences;
import com.example.barcodescanner.tools.BarcodeHandler;
import com.example.barcodescanner.tools.JsonRequest;
import com.example.barcodescanner.tools.SidebarHandler;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

import java.util.List;


public class WH_Export_Bruss_Activity extends AppCompatActivity {
    public TextView barcodeResultTextView;
    public TextView serverResponseTextView;
    public TableLayout tableLayout;
    public AppPreferences appPreferences;
    private ActionBarDrawerToggle drawerToggle;
    public final String currentLine = "Magazyn (Kolejowa) Bruss WZ";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wh_export_bruss);

        // Use the preference
        // Auto Send
        appPreferences = new AppPreferences(this);

        CheckBox checkBoxVariable = findViewById(R.id.checkBoxVariable);
        checkBoxVariable.setChecked(appPreferences.getAutoSendState());
        checkBoxVariable.setOnCheckedChangeListener((buttonView, isChecked) -> {
            // Update the variable state
            appPreferences.setAutoSendState(isChecked);
        });

        Button retrieveValuesButton = findViewById(R.id.retrieveValuesButton);

        // Set an OnClickListener for the button
        retrieveValuesButton.setOnClickListener(v -> {
            // Retrieve stored values and display them in an alert or dialog
            List<String> storedValues = appPreferences.getUnsentCodes();
            sidebarAlert(storedValues);
        });

        // Barcode input field
        EditText barcodeEditText = findViewById(R.id.barcodeEditText);
        barcodeEditText.setInputType(InputType.TYPE_NULL);

        barcodeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // This method is called to notify you that somewhere within s, the text is about to be replaced.
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // This method is called to notify you that somewhere within s, the text has been replaced.
            }

            @Override
            public void afterTextChanged(Editable editable) {
                // This method is called to notify you that the characters within s are about to be replaced with new text.
            }
        });

        barcodeEditText.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            int scanCode;
            if (appPreferences.getAutoSendState()) {
                scanCode = 0;
            } else {
                scanCode = 17;
            }
            if (actionId == EditorInfo.IME_ACTION_DONE ||
                    (keyEvent != null && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN && keyEvent.getScanCode() == scanCode)) {
                // Enter key pressed or action button pressed
                String scannedCode = barcodeEditText.getText().toString();
                String scannedCode2 = "";
                // Process the scanned code, e.g., send it to your server
                sendBarcodeRequest(scannedCode, scannedCode2);
                // Clear the EditText for the next scan
                barcodeEditText.setText("");

                // Use View.post to ensure that the focus is set after the view is fully initialized
                runOnUiThread(() -> barcodeEditText.post(barcodeEditText::requestFocus));
                return true;
            }
            return false;
        });
        // Initial focus
        runOnUiThread(() -> barcodeEditText.post(barcodeEditText::requestFocus));

        // For testing purposes (device without build-in skanner)
        // barcodeEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        // barcodeEditText.setSingleLine(true);

        // Barcode responses
        barcodeResultTextView = findViewById(R.id.barcodeResultTextView);
        serverResponseTextView = findViewById(R.id.serverResponseTextView);

        // Table displaying successful scans
        List<List<String>> allLists = appPreferences.getWHlist();


        tableLayout = findViewById(R.id.tableLayout);
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);

        for (List<String> innerList : allLists) {
            if (innerList.size() > 2) {
                TableRow tableRow = new TableRow(this);
                tableRow.setLayoutParams(layoutParams);
                tableRow.addView(createTextView(innerList.get(0)));
                tableRow.addView(createTextView(innerList.get(2)));

                tableLayout.addView(tableRow, 0);
            }
        }
    }
    private void sendBarcodeRequest(String barcodeData, String barcodeData2) {
        // Pass the callback to the BarcodeRequestTask
        BarcodeHandler barcodeHandler = new BarcodeHandler(this, tableLayout, barcodeData, barcodeData2,"", currentLine, barcodeResultTextView, serverResponseTextView);
        barcodeHandler.sendBarcodeRequest();
    }
    private void sidebarAlert(List<String> storedValues) {
        SidebarHandler sidebarHandler = new SidebarHandler(this, tableLayout, appPreferences, currentLine, barcodeResultTextView, serverResponseTextView);
        sidebarHandler.showAlertDialog(storedValues);
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        return drawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    private TextView createTextView(String text) {
        TextView textView = new TextView(this);
        textView.setText(text);
        textView.setTextSize(20);
        textView.setGravity(Gravity.CENTER);
        textView.setPadding(8, 8, 8, 24);
        return textView;
    }

    public void deleteLatest(View view) {
        MaterialAlertDialogBuilder builder;
        builder = new MaterialAlertDialogBuilder(this, R.style.AlertDialogStyleDefault);
        builder.setTitle("Potwierdź")
                .setMessage("Usunąć ostatni kod?")
                .setPositiveButton("Tak", (dialog, which) -> {
                    appPreferences.deleteLatestWHRecord();
                    List<List<String>> allLists = appPreferences.getWHlist();

                    tableLayout.removeAllViews();
            
                    TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(
                            TableRow.LayoutParams.MATCH_PARENT,
                            TableRow.LayoutParams.WRAP_CONTENT);

                    for (List<String> innerList : allLists) {
                        if (innerList.size() > 2) {
                            TableRow tableRow = new TableRow(this);
                            tableRow.setLayoutParams(layoutParams);
                            tableRow.addView(createTextView(innerList.get(0)));
                            tableRow.addView(createTextView(innerList.get(2)));

                            tableLayout.addView(tableRow, 0);
                        }
                    }
                })
                .setNegativeButton("Nie", (dialog, which) ->{
                })
                .show();
    }

    public void sendJson(View view) {
        MaterialAlertDialogBuilder builder;
        builder = new MaterialAlertDialogBuilder(this, R.style.AlertDialogStyleDefault);
        builder.setTitle("Potwierdź")
                .setMessage("Wysłać dane?")
                .setPositiveButton("Tak", (dialog, which) -> {
                    // Assuming you have an instance of SharedPreferences named preferences
                    JsonRequest httpClient = new JsonRequest();
                    List<List<String>> whList = appPreferences.getWHlist();

                    if (whList.isEmpty()) {
                        MaterialAlertDialogBuilder builder2;
                        builder2 = new MaterialAlertDialogBuilder(this, R.style.AlertDialogStyleYellow);
                        builder2.setTitle("Uwaga")
                                .setMessage("Brak kodów do wysłania!")
                                .setPositiveButton("OK", (dialog2, which2) -> { })
                                .show();
                    } else {
                        // Execute the HTTP request in the background
                        new SendHttpRequestTask(tableLayout, appPreferences, this).execute(httpClient, whList);
                    }

                })
                .setNegativeButton("Nie", (dialog, which) ->{
                })
                .show();
   }

    private class SendHttpRequestTask extends AsyncTask<Object, Void, Void> {
        private WeakReference<Context> contextRef;

        SendHttpRequestTask(TableLayout tableLayout, AppPreferences appPreferences, Context context) {
            contextRef = new WeakReference<>(context);
        }
        @Override
        protected Void doInBackground(Object... params) {
            if (params.length == 2) {
                JsonRequest httpClient = (JsonRequest) params[0];
                List<List<String>> whList = (List<List<String>>) params[1];

                // Execute the HTTP request
                httpClient.sendWHListAsHttpRequest(whList, new JsonRequest.HttpRequestCallback() {
                    @Override
                    public void onResponse(JSONObject serverResponse) {
                        runOnUiThread(() -> {
                            // Handle the server response as needed
                            Context context = contextRef.get();

                            MaterialAlertDialogBuilder builder = null;

                            String status;
                            String message;

                            String title = null;

                            try {
                                status = serverResponse.getString("status");
                                message = serverResponse.getString("message");
                            } catch (JSONException e) {
                                status = "Błąd";
                                message = String.valueOf(e);
                            }

                            switch (status) {
                                case "SUCCESS":
                                    if (tableLayout != null && context != null) {
                                        // Clear the TableLayout after the HTTP request is completed
                                        tableLayout.removeAllViews();
                                        appPreferences.clearWHValues();
                                    }
                                    title = "Sukces";
                                    builder = new MaterialAlertDialogBuilder(context, R.style.AlertDialogStyleGreen);
                                    break;
                                case "WARNING":
                                    title = "Uwaga";
                                    builder = new MaterialAlertDialogBuilder(context, R.style.AlertDialogStyleYellow);
                                    break;
                                case "FAILURE":
                                    title = "Błąd";
                                    builder = new MaterialAlertDialogBuilder(context, R.style.AlertDialogStyleRed);
                                    break;
                            }

                            builder.setTitle(title)
                                    .setMessage(message)
                                    .setPositiveButton("OK", (dialog, which) -> {
                                        // You can perform additional actions here if needed
                                    })
                                    .show();
                        });
                    }

                    @Override
                    public void onFailure(String errorMessage) {
                        // Handle the failure/error as needed
                        runOnUiThread(() -> {
                            // Handle the server response as needed
                            Context context = contextRef.get();
                            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(context, R.style.AlertDialogStyleRed);

                            builder.setTitle("Błąd")
                                    .setMessage("Brak połączenia")
                                    .setPositiveButton("OK", (dialog, which) -> {
                                        // You can perform additional actions here if needed
                                    })
                                    .show();
                        });
                    }
                });
            }
            return null;
        }

    }
}

