package com.example.barcodescanner.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.barcodescanner.R;
import com.example.barcodescanner.activities.nicr.NiCrActivity;
import com.example.barcodescanner.activities.nicr.NiCrLineIActivity;
import com.example.barcodescanner.activities.qc.QCActivity;
import com.example.barcodescanner.activities.qc.QCKTLActivity;
import com.example.barcodescanner.activities.warehouse.WHActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button NiCrButton = findViewById(R.id.NiCr);
        Button PButton = findViewById(R.id.P);
        Button ZnButton = findViewById(R.id.Zn);
        Button PressButton = findViewById(R.id.Press);
        Button KTLButton = findViewById(R.id.KTL);
        Button WHButton = findViewById(R.id.WH);
        Button QCButton = findViewById(R.id.QC);
        Button StatusButton = findViewById(R.id.Status);

        NiCrButton.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, NiCrActivity.class);
            startActivity(intent);
        });

        PButton.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, PActivity.class);
            startActivity(intent);
        });

        ZnButton.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, ZnActivity.class);
            startActivity(intent);
        });

        PressButton.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, PressActivity.class);
            startActivity(intent);
        });

        KTLButton.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, KTLActivity.class);
            startActivity(intent);
        });

        WHButton.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, WHActivity.class);
            startActivity(intent);
        });

        QCButton.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, QCActivity.class);
            startActivity(intent);
        });

        StatusButton.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, StatusActivity.class);
            startActivity(intent);
        });

    }

}
