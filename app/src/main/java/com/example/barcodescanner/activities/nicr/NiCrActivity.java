package com.example.barcodescanner.activities.nicr;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.example.barcodescanner.R;
import com.example.barcodescanner.activities.warehouse.WHActivity;
import com.example.barcodescanner.activities.warehouse.WH_ExportActivity;
import com.example.barcodescanner.activities.warehouse.WH_ImportActivity;

public class NiCrActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ni_cr);
        Button LineIButton = findViewById(R.id.LineIButton);
        Button LineIIButton = findViewById(R.id.LineIIButton);

        LineIButton.setOnClickListener(v -> {
            Intent intent = new Intent(NiCrActivity.this, NiCrLineIActivity.class);
            startActivity(intent);
        });

        LineIIButton.setOnClickListener(v -> {
            Intent intent = new Intent(NiCrActivity.this, NiCrLineIIActivity.class);
            startActivity(intent);
        });
    }
}