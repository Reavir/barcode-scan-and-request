package com.example.barcodescanner.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AppPreferences {
    private static final String PREF_NAME = "MyAppPreferences";
    private static final String KEY_AUTO_SEND_STATE = "auto_send_state";
    private static final String KEY_UNSENT_CODES_STATE = "unsent_codes_state";
    private static final String DELIMITER = ",";
    private static final String WH_KEY_LIST = "warehouse_state";
    private static final String WH_OTHER_KEY_LIST = "warehouse_other_state";
    private SharedPreferences preferences;

    public AppPreferences(Context context) {
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public boolean getAutoSendState() {
        return preferences.getBoolean(KEY_AUTO_SEND_STATE, false);
    }
    public void setAutoSendState(boolean state) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(KEY_AUTO_SEND_STATE, state);
        editor.apply();
    }
    public List<String> getUnsentCodes() {
        String serializedString = preferences.getString(KEY_UNSENT_CODES_STATE, null);

        if (serializedString != null) {
            // Split the string only if it's not null
            return Arrays.asList(TextUtils.split(serializedString, DELIMITER));
        } else {
            // Return an empty list or handle the null case based on your requirements
            return new ArrayList<>();
        }
    }
    public void setUnsentCodes(List<String> state) {
        String serializedString = TextUtils.join(DELIMITER, state);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_UNSENT_CODES_STATE, serializedString);
        editor.apply();
    }
    public void clearSharedPreferences() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(KEY_UNSENT_CODES_STATE);  // Remove the stored data
        editor.apply();
    }
    public void clearCodes(List<String> valuesToClear) {
        // Retrieve the existing serialized string
        String serializedString = preferences.getString(KEY_UNSENT_CODES_STATE, null);

        if (serializedString != null) {
            // Convert the serialized string to a mutable list
            List<String> existingValues = new ArrayList<>(Arrays.asList(TextUtils.split(serializedString, DELIMITER)));

            // Remove checked values from the existing list
            existingValues.removeAll(valuesToClear);

            // Convert the list back to a serialized string
            String updatedSerializedString = TextUtils.join(DELIMITER, existingValues);

            // Save the updated serialized string back to SharedPreferences
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(KEY_UNSENT_CODES_STATE, updatedSerializedString);
            editor.apply();
        }
    }
    public void setWHlist(List<List<String>> values) {
        SharedPreferences.Editor editor = preferences.edit();
        StringBuilder stringBuilder = new StringBuilder();

        for (List<String> innerList : values) {
            for (String item : innerList) {
                stringBuilder.append(item).append(DELIMITER);
            }
            // Separate inner lists with another character, e.g., "|"
            stringBuilder.append("|");
        }

        editor.putString(WH_KEY_LIST, stringBuilder.toString());
        editor.apply();
    }
    public void setWHlistOther(List<List<String>> values) {
        SharedPreferences.Editor editor = preferences.edit();
        StringBuilder stringBuilder = new StringBuilder();

        if (values.size() > 50) {
            values = values.subList(values.size() - 50, values.size());
        }

        for (List<String> innerList : values) {
            for (String item : innerList) {
                stringBuilder.append(item).append(DELIMITER);
            }
            // Separate inner lists with another character, e.g., "|"
            stringBuilder.append("|");
        }

        editor.putString(WH_OTHER_KEY_LIST, stringBuilder.toString());
        editor.apply();
    }
    public List<List<String>> getWHlist() {
        String storedValue = preferences.getString(WH_KEY_LIST, null);
        List<List<String>> result = new ArrayList<>();

        if (storedValue != null) {
            String[] outerArray = storedValue.split("\\|");
            for (String outerString : outerArray) {
                String[] innerArray = outerString.split(DELIMITER);
                List<String> innerList = new ArrayList<>(Arrays.asList(innerArray));
                if (!innerList.isEmpty() && !innerList.toString().equals("[]")) {
                    result.add(innerList);
                }
            }
        }

        return result;
    }
    public List<List<String>> getWHlistOther() {
        String storedValue = preferences.getString(WH_OTHER_KEY_LIST, null);
        List<List<String>> result = new ArrayList<>();

        if (storedValue != null) {
            String[] outerArray = storedValue.split("\\|");
            for (String outerString : outerArray) {
                String[] innerArray = outerString.split(DELIMITER);
                List<String> innerList = new ArrayList<>(Arrays.asList(innerArray));
                result.add(innerList);
            }
        }
        return result;
    }
    public void clearWHOtherValues() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(WH_OTHER_KEY_LIST);
        editor.apply();
    }
    public void clearWHValues() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(WH_KEY_LIST);
        editor.apply();
    }
    public boolean containsWHList(List<String> targetList , List<List<String>> inputList) {
        for (List<String> innerList : inputList) {
            if (innerList.equals(targetList)) {
                return true;
            }
        }

        return false;
    }
    public void deleteLatestWHOtherRecord() {
        List<List<String>> whoList = getWHlistOther();

        if (!whoList.isEmpty()) {
            // Remove the last record from the list
            whoList.remove(whoList.size() - 1);

            // Save the updated list to SharedPreferences
            setWHlistOther(whoList);
        }
    }
    public void deleteLatestWHRecord() {
        List<List<String>> whList = getWHlist();

        if (!whList.isEmpty()) {
            // Remove the last record from the list
            whList.remove(whList.size() - 1);

            // Save the updated list to SharedPreferences
            setWHlist(whList);
        }
    }
}

