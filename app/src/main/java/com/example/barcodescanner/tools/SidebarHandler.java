package com.example.barcodescanner.tools;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.example.barcodescanner.R;
import com.example.barcodescanner.preferences.AppPreferences;

public class SidebarHandler {
    Context context;
    TableLayout tableLayout;
    String currentLine;
    TextView barcodeResultTextView;
    TextView serverResponseTextView;
    AppPreferences appPreferences;

    public SidebarHandler(Context inpContext, TableLayout inpTableLayout, AppPreferences inpPref, String inpLine,
                          TextView inpBarcodeResult, TextView inpServerResponse) {
        this.context = inpContext;
        this.tableLayout = inpTableLayout;
        this.appPreferences = inpPref;
        this.currentLine = inpLine;
        this.barcodeResultTextView = inpBarcodeResult;
        this.serverResponseTextView = inpServerResponse;
    }
    public void showAlertDialog(List<String> storedValues) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogStyleDefault);
        builder.setTitle("Zapisane dane");

        if (storedValues != null && !storedValues.isEmpty()) {
            // Convert the List<String> to an array for use in the dialog
            final CharSequence[] valuesArray = storedValues.toArray(new CharSequence[0]);
            // Make a copy of the values array to modify the displayed strings
            final CharSequence[] displayValuesArray = Arrays.copyOf(valuesArray, valuesArray.length);

            // Modify the displayed strings in displayValuesArray to extract "X Y Z"
            for (int i = 0; i < displayValuesArray.length; i++) {
                // Extract "X Y Z" from the original string
                String originalString = displayValuesArray[i].toString();

                // Extract values from the original string (assuming the format is consistent)
                String[] parts = originalString.split("&");
                String[] values = new String[parts.length];
                for (int j = 0; j < parts.length; j++) {
                    String[] keyValue = parts[j].split("=");
                    values[j] = keyValue.length > 1 ? keyValue[1] : "";
                }

                // Create the modified string "X Y Z"
                String modifiedString = TextUtils.join(" ", values);

                // Assign the modified string back to displayValuesArray
                displayValuesArray[i] = modifiedString;
            }

            // Array to track checked items (all initially checked)
            boolean[] checkedItems = new boolean[valuesArray.length];
            Arrays.fill(checkedItems, true);

            // Set a multi-choice items dialog with the stored values
            builder.setMultiChoiceItems(displayValuesArray, checkedItems, (dialog, which, isChecked) -> {
                // Handle item click if needed
            });
            // Set a positive button (OK) to close the dialog
            builder.setPositiveButton("OK", (dialog, which) -> {
                class MutableValues {
                    String xValue = "";
                    String yValue = "";
                    String zValue = "";
                }
                Handler handler = new Handler();
                for (int i = 0; i < checkedItems.length; i++) {
                    if (checkedItems[i]) {
                        String checkedValue = valuesArray[i].toString();
                        String[] parts = checkedValue.split("&");

                        final MutableValues mutableValues = new MutableValues();

                        for (String part : parts) {
                            String[] keyValue = part.split("=");
                            if (keyValue.length > 1) {
                                // Check for the key and assign the value to the corresponding variable
                                if ("barcodeData".equals(keyValue[0])) {
                                    mutableValues.xValue = keyValue[1];
                                } else if ("barcodeData2".equals(keyValue[0])) {
                                    mutableValues.yValue = keyValue[1];
                                } else if ("barcodeData3".equals(keyValue[0])) {
                                    mutableValues.zValue = keyValue[1];
                                }
                            }
                        }
                        // Use a handler to delay the execution of sendBarcodeRequest
                        handler.postDelayed(() -> sendBarcodeRequest(mutableValues.xValue, mutableValues.yValue, mutableValues.zValue), i * 5000L);
                    }
                }
                clearCheckedItems(valuesArray, checkedItems);
            });

            // Set a negative button (Clear) to show a confirmation dialog before clearing the data
            builder.setNegativeButton("Wyczyść", (dialog, which) -> {
                // Show a confirmation dialog before clearing the data
                showClearConfirmationDialog(valuesArray, checkedItems);
            });
        } else {
            builder.setMessage("Brak zapisanych danych");
            // Set a positive button (OK) to close the dialog
            builder.setPositiveButton("OK", (dialog, which) -> {
                // Handle OK button click if needed
            });
        }

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
    // Method to show a confirmation dialog before clearing the data
    public void showClearAlertDialog() {
        AlertDialog.Builder confirmationBuilder = new AlertDialog.Builder(context, R.style.AlertDialogStyleDefault);
        confirmationBuilder.setTitle("Potwierdzenie");
        confirmationBuilder.setMessage("Czy chcesz usunąć dane?");

        // Set a positive button (Yes) to clear the data
        confirmationBuilder.setPositiveButton("Tak", (dialog, which) -> {
            try {
                // Attempt to clear the data
                appPreferences.clearWHOtherValues();
                tableLayout.removeAllViews();
                // Show a success alert
                showClearedAlert("Sukces", "Dane usunięte");
            } catch (Exception e) {
                // Show an error alert
                showClearedAlert("Błąd", "Błąd usuwania danych: " + e.getMessage());
            }
        });

        // Set a negative button (No) to cancel the operation
        confirmationBuilder.setNegativeButton("Nie", null);

        // Create and show the confirmation dialog
        AlertDialog confirmationDialog = confirmationBuilder.create();
        confirmationDialog.show();
    }
    private void showClearConfirmationDialog(final CharSequence[] valuesArray, final boolean[] checkedItems) {
        AlertDialog.Builder confirmationBuilder = new AlertDialog.Builder(context, R.style.AlertDialogStyleDefault);
        confirmationBuilder.setTitle("Potwierdzenie");
        confirmationBuilder.setMessage("Czy chcesz usunąć dane?");

        // Set a positive button (Yes) to clear the data
        confirmationBuilder.setPositiveButton("Tak", (dialog, which) -> {
            try {
                // Attempt to clear the data
                clearCheckedItems(valuesArray, checkedItems);
                // Show a success alert
                showClearedAlert("Sukces", "Dane usunięte");
            } catch (Exception e) {
                // Show an error alert
                showClearedAlert("Błąd", "Błąd usuwania danych: " + e.getMessage());
            }
        });

        // Set a negative button (No) to cancel the operation
        confirmationBuilder.setNegativeButton("Nie", null);

        // Create and show the confirmation dialog
        AlertDialog confirmationDialog = confirmationBuilder.create();
        confirmationDialog.show();
    }
    // Method to clear the checked items
    private void clearCheckedItems(CharSequence[] valuesArray, boolean[] checkedItems) {
        List<String> checkedValues = new ArrayList<>();

        for (int i = 0; i < checkedItems.length; i++) {
            if (checkedItems[i]) {
                checkedValues.add(valuesArray[i].toString());
            }
        }
        appPreferences.clearCodes(checkedValues);
    }
    private void showClearedAlert(String title, String message) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context, R.style.AlertDialogStyleGreen);
        alertBuilder.setTitle(title);
        alertBuilder.setMessage(message);
        alertBuilder.setPositiveButton("OK", null);
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }
    public void showDeleteAlert(String barcodeData, String barcodeData2, String barcodeData3, TableRow.LayoutParams layoutParams) {
        AlertDialog.Builder confirmationBuilder = new AlertDialog.Builder(context, R.style.AlertDialogStyleDefault);
        confirmationBuilder.setTitle("Potwierdzenie");
        confirmationBuilder.setMessage(String.format("Czy chcesz usunąć %s %s", barcodeData, barcodeData2));

        // Set a positive button (Yes) to clear the data
        confirmationBuilder.setPositiveButton("Tak", (dialog, which) -> {
            try {
                // Attempt to clear the data
                sendBarcodeRequest(barcodeData, barcodeData2, barcodeData3);

                appPreferences.deleteLatestWHOtherRecord();

                List<List<String>> allLists = appPreferences.getWHlistOther();
                UtilsUI.appendTable(allLists, tableLayout, layoutParams, context);

            } catch (Exception e) {
                // Show an error alert
                showClearedAlert("Błąd", "Błąd usuwania danych: " + e.getMessage());
            }
        });

        // Set a negative button (No) to cancel the operation
        confirmationBuilder.setNegativeButton("Nie", null);

        // Create and show the confirmation dialog
        AlertDialog confirmationDialog = confirmationBuilder.create();
        confirmationDialog.show();
    }
    private void sendBarcodeRequest(String barcodeData, String barcodeData2, String barcodeData3) {
        // Pass the callback to the BarcodeRequestTask
        BarcodeHandler barcodeHandler = new BarcodeHandler(context, tableLayout, barcodeData, barcodeData2, barcodeData3, currentLine, barcodeResultTextView, serverResponseTextView);
        barcodeHandler.sendBarcodeRequest();
    }
}
