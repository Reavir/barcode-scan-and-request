package com.example.barcodescanner.tools;

import androidx.annotation.NonNull;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

public class JsonRequest {

    private static final String YOUR_API_ENDPOINT = "https://v06wnnc1-8000.euw.devtunnels.ms/tools/barcodes/api/post/";
    private static final MediaType JSON_MEDIA_TYPE = MediaType.parse("application/json; charset=utf-8");

    public interface HttpRequestCallback {
        void onResponse(JSONObject serverResponse);

        void onFailure(String errorMessage);
    }
    public void sendWHListAsHttpRequest(List<List<String>> whList, HttpRequestCallback callback) {
        try {
            JSONArray jsonArray = new JSONArray();

            for (List<String> innerList : whList) {
                JSONArray innerArray = new JSONArray(innerList);
                jsonArray.put(innerArray);
            }

            JSONObject requestBodyJson = new JSONObject();
            requestBodyJson.put("jsonData", jsonArray);

            OkHttpClient client = new OkHttpClient();
            RequestBody requestBody = RequestBody.create(JSON_MEDIA_TYPE, requestBodyJson.toString());

            Request request = new Request.Builder()
                    .url(YOUR_API_ENDPOINT)
                    .post(requestBody)
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                    if (response.isSuccessful()) {
                        String responseBody = response.body().string();
                        try {
                            JSONObject jsonResponse = new JSONObject(responseBody);
                            callback.onResponse(jsonResponse);

                        } catch (JSONException e) {
                            throw new RuntimeException(e);
                        }
                    } else {
                        callback.onFailure("Server error: " + response.code());
                    }
                }

                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    callback.onFailure("Request failed: " + e.getMessage());
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
            callback.onFailure("JSON error: " + e.getMessage());
        }
    }
}
