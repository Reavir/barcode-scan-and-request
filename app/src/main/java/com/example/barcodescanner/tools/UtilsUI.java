package com.example.barcodescanner.tools;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.view.Gravity;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.example.barcodescanner.R;

import java.util.List;

public class UtilsUI {
    // Static method to create a TextView with equal column weight
    public static TextView createTextView(String text, Context context) {
        TextView textView = new TextView(context);
        textView.setText(text);
        textView.setTextSize(20);
        textView.setGravity(Gravity.CENTER);
        textView.setPadding(4, 4, 4, 12);

        // Set layout params for equal column width
        TableRow.LayoutParams params = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 1f);
        textView.setLayoutParams(params); // Ensure equal width for all columns

        return textView;
    }
    public static void appendTable(List<List<String>> lists, TableLayout tableLayout, TableRow.LayoutParams layoutParams, Context context) {
        tableLayout.removeAllViews();

        for (List<String> innerList : lists) {
            if (innerList.size() > 2) {
                TableRow tableRow = new TableRow(context);
                tableRow.setLayoutParams(layoutParams);

                // Create TextViews for each element
                tableRow.addView(UtilsUI.createTextView(getFirstColumnText(innerList), context));
                tableRow.addView(UtilsUI.createTextView(innerList.get(1), context));
                tableRow.addView(UtilsUI.createTextView(innerList.get(2), context));

                tableLayout.addView(tableRow, 0);
            }
        }
    }
    @NonNull
    private static String getFirstColumnText(List<String> innerList) {
        String firstColumnText = innerList.get(0);
        if (firstColumnText.length() == 9
                && firstColumnText.substring(0, 3).matches("[a-zA-Z]+")  // Check if first 3 characters are letters
                && firstColumnText.substring(3, 9).matches("\\d{6}")) {  // Check if last 6 characters are digits
            firstColumnText = firstColumnText.substring(0, 3) + "\n" + firstColumnText.substring(3); // Insert newline
        }
        return firstColumnText;
    }

    public static void setEditTextBackground(boolean hasFocus, EditText barcodeText, Context context) {
        if (hasFocus) {
            Drawable drawable = ContextCompat.getDrawable(context, R.drawable.edit_text_background_focus); // Your custom drawable
            // Change the appearance when EditText gains focus
            if (drawable instanceof GradientDrawable) {
                GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                gradientDrawable.setStroke(2, ContextCompat.getColor(context, android.R.color.holo_green_light)); // Example: Set a colored border
                barcodeText.setBackground(drawable);
            }
        } else {
            Drawable drawable = ContextCompat.getDrawable(context, R.drawable.edit_text_background_default); // Your custom drawable
            // Revert back to normal appearance when EditText loses focus
            if (drawable instanceof GradientDrawable) {
                GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                gradientDrawable.setStroke(2, ContextCompat.getColor(context, android.R.color.transparent)); // Example: Set default border color
                barcodeText.setBackground(drawable);
            }
        }
    }
}