package com.example.barcodescanner.tools;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;

import com.example.barcodescanner.preferences.AppPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class BarcodeRequestTask extends AsyncTask<String, Void, String> {

    private final BarcodeRequestCallback callback;
    private AppPreferences appPreferences;

    public interface BarcodeRequestCallback {
        void onBarcodeRequestComplete(String result);
    }
    public BarcodeRequestTask(Context context, BarcodeRequestCallback callback) {
        this.appPreferences = new AppPreferences(context);
        this.callback = callback;
    }
    @SuppressWarnings("deprecation")
    @Override
    protected String doInBackground(@NonNull String... params) {
        try {
            String apiUrl = params[0];
            String selectedOption = params[1];
            String timestamp = params[2];

            String barcodeData = params[3];
            String barcodeData2 = params[4];
            String barcodeData3 = params[5];

            String requestData;
            URL url = new URL(apiUrl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            try {
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("ngrok-skip-browser-warning", "1");

                requestData = "barcodeData=" + URLEncoder.encode(barcodeData, "UTF-8") +
                        "&barcodeData2=" + URLEncoder.encode(barcodeData2, "UTF-8") +
                        "&barcodeData3=" + URLEncoder.encode(barcodeData3, "UTF-8") +
                        "&selectedOption=" + URLEncoder.encode(selectedOption, "UTF-8") +
                        "&timestamp=" + URLEncoder.encode(timestamp, "UTF-8");

                try (OutputStream outputStream = urlConnection.getOutputStream();
                     BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8))) {
                    writer.write(requestData);

                } catch (IOException e) {
                    List<String> storedStrings = new ArrayList<>(appPreferences.getUnsentCodes());
                    if (!storedStrings.contains(requestData)) {
                        storedStrings.add(requestData);
                        appPreferences.setUnsentCodes(storedStrings);
                    }
                    if (Objects.requireNonNull(e.getMessage()).contains("No address associated with hostname")) {
                        JSONObject errorResponse = new JSONObject();
                        errorResponse.put("status", "WARNING");
                        errorResponse.put("message", "Kod został zapisany.\nBrak połączenia z internetem");

                        return errorResponse.toString();
                    }
                }

                int responseCode = urlConnection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    // Read and process the response if needed
                    InputStream inputStream = urlConnection.getInputStream();
                    // Process the input stream as needed
                    return readStream(inputStream);
                } else {
                    // Handle error cases
                    List<String> storedStrings = new ArrayList<>(appPreferences.getUnsentCodes());
                    if (!storedStrings.contains(requestData)) {
                        storedStrings.add(requestData);
                        appPreferences.setUnsentCodes(storedStrings);
                    }

                    JSONObject errorResponse = new JSONObject();
                    errorResponse.put("status", "WARNING");

                    if (responseCode == HttpURLConnection.HTTP_BAD_GATEWAY) {
                        errorResponse.put("message", "Kod został zapisany.\nSerwer nieaktywny.");
                    } else if (responseCode == HttpURLConnection.HTTP_NOT_FOUND) {
                        errorResponse.put("message", "Kod został zapisany.\nSerwer wyłączony.");
                    } else {
                        errorResponse.put("message", "HTTP Error: " + responseCode);
                    }

                    return errorResponse.toString();
                }
            } finally {
                urlConnection.disconnect();
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            JSONObject errorResponse = new JSONObject();
            try {
                errorResponse.put("status", "FAILURE");
                errorResponse.put("message", "Error: " + e.getMessage());
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            return errorResponse.toString();
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (callback != null) {
            callback.onBarcodeRequestComplete(result);
        }
    }

    private String readStream(InputStream inputStream) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            return stringBuilder.toString();
        }
    }
}
