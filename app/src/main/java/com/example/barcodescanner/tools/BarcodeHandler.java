package com.example.barcodescanner.tools;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.barcodescanner.R;
import com.example.barcodescanner.preferences.AppPreferences;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BarcodeHandler {
    private final Context context;
    private final TableLayout tableLayout;
    private final String line;
    private final TextView barcodeResultTextView;
    private final TextView serverResponseTextView;
    private final String codeData;
    private final String codeData2;
    private final String codeData3;
    private final String timestamp;
    private final String time;

    public AppPreferences appPreferences;
    public BarcodeHandler(Context inpContext, TableLayout inpTableLayout, String barcodeData, String barcodeData2, String barcodeData3,
                               String inpLine, TextView inpBarcodeResult, TextView inpServerResponse) {
        this.context = inpContext;
        this.tableLayout = inpTableLayout;
        this.line = inpLine;
        this.barcodeResultTextView = inpBarcodeResult;
        this.serverResponseTextView = inpServerResponse;
        this.codeData = barcodeData;
        this.codeData2 = barcodeData2;
        this.codeData3 = barcodeData3;
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        this.timestamp = now.format(formatter);
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        this.time = now.format(timeFormatter);
        appPreferences = new AppPreferences(inpContext);
    }

    public void sendBarcodeRequest() {
        String apiUrl = "https://v06wnnc1-8000.euw.devtunnels.ms/tools/barcodes/api/post/";

        // Pass the callback to the BarcodeRequestTask
        new BarcodeRequestTask(context, result -> handleBarcodeResponse(result, codeData, codeData2, codeData3, false))
                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, apiUrl, line, timestamp, codeData, codeData2, codeData3);

        List<String> storedValues = appPreferences.getUnsentCodes();
        if (!storedValues.isEmpty() && !Objects.equals(line, "Magazyn (Kolejowa) Bruss WZ")) {
            if (ConnectionCheck.isConnectedToInternet(context)) {
                if (ConnectionCheck.checkURL(apiUrl)) {
                    // Send codes from the unsent list

                    final CharSequence[] valuesArray = storedValues.toArray(new CharSequence[0]);
                    class MutableValues {
                        String xValue = "";
                        String yValue = "";
                        String zValue = "";
                    }

                    // Define a method for recursive task execution
                    Runnable executeNextTask = new Runnable() {
                        int currentIndex = 0;

                        @Override
                        public void run() {
                            if (currentIndex < valuesArray.length) {
                                String checkedValue = valuesArray[currentIndex].toString();
                                String[] parts = checkedValue.split("&");

                                final MutableValues mutableValues = new MutableValues();

                                for (String part : parts) {
                                    String[] keyValue = part.split("=");
                                    if (keyValue.length > 1) {
                                        // Check for the key and assign the value to the corresponding variable
                                        if ("barcodeData".equals(keyValue[0])) {
                                            mutableValues.xValue = keyValue[1];
                                        } else if ("barcodeData2".equals(keyValue[0])) {
                                            mutableValues.yValue = keyValue[1];
                                        } else if ("barcodeData3".equals(keyValue[0])) {
                                            mutableValues.zValue = keyValue[1];
                                        }
                                    }
                                }

                                new BarcodeRequestTask(context, result -> handleBarcodeResponse(result, mutableValues.xValue, mutableValues.yValue, mutableValues.zValue, true))
                                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, apiUrl, line, timestamp, mutableValues.xValue, mutableValues.yValue, mutableValues.zValue);

                                // Schedule the next task after a delay
                                new Handler(Looper.getMainLooper()).postDelayed(this, 5000);
                                currentIndex++; // Move to the next index
                            } else {
                                // All tasks executed, clear preferences if needed
                                appPreferences.clearSharedPreferences();
                            }
                        }
                    };

                    // Start the recursive execution
                    executeNextTask.run();
                }
            }
        }
    }
    private void handleBarcodeResponse(String result, String barcodeData, String barcodeData2, String barcodeData3, Boolean toast) {
        try {
            if (Objects.equals(line, "Status")) {
                JSONObject jsonResponse = new JSONObject(result);
                // Extract values from JSON
                String latestScan = jsonResponse.optString("latestScan", "");
                String remainingQuantity = jsonResponse.optString("remainingQuantity", "");

                barcodeResultTextView.setText(String.format("Kod: %s", barcodeData));
                serverResponseTextView.setText(String.format("Ostatni skan: %s, %s", latestScan, remainingQuantity));

                addRowToTable(barcodeData, remainingQuantity, "0");

            } else {
                JSONObject jsonResponse = new JSONObject(result);
                // Extract values from JSON
                String status = jsonResponse.optString("status", "");
                String message = jsonResponse.optString("message", "");
                String packaging = jsonResponse.optString("packaging", "");
                String quantity = jsonResponse.optString("quantity", "");

                barcodeResultTextView.setText(String.format("Kod: %s", barcodeData));
                serverResponseTextView.setText(message);
                switch (status) {
                    case "SUCCESS":
                        if(!toast) {
                            showAlertDialog("Sukces", message, "green", packaging);
                        } else {
                            Toast toast1 = Toast.makeText(context, "Sukces " + message, Toast.LENGTH_SHORT);
                            toast1.show();
                        }
                        addRowToTable(barcodeData, barcodeData2, quantity);
                        break;
                    case "WARNING":
                        if(!toast) {
                            showAlertDialog("Uwaga", message, "yellow", packaging);
                        } else {
                            Toast toast1 = Toast.makeText(context, "Uwaga " + message, Toast.LENGTH_SHORT);
                            toast1.show();
                        }
                        break;
                    case "FAILURE":
                        if(!toast) {
                            showAlertDialog("Niepowodzenie", message, "red", packaging);
                        } else {
                            Toast toast1 = Toast.makeText(context, "Niepowodzenie " + message, Toast.LENGTH_SHORT);
                            toast1.show();
                        }
                        break;
                }
            }
        } catch (
                JSONException e) {
            e.printStackTrace();
        }
    }
    private void addRowToTable(String barcodeData, String barcodeData2, String quantity) {
        if (line.contains("Magazyn")) {
            appPreferences = new AppPreferences(context);

            List<String> newScan = new ArrayList<>();
            newScan.add(barcodeData);
            newScan.add(barcodeData2);
            newScan.add(time);
            List<List<String>> allLists;
            if (line.contains("Bruss")) {
                allLists = appPreferences.getWHlist();
            } else {
                allLists = appPreferences.getWHlistOther();
            }


            if (!appPreferences.containsWHList(newScan, allLists)){
                allLists.add(newScan);
                if (line.contains("Bruss")) {
                    appPreferences.setWHlist(allLists);
                } else {
                    appPreferences.setWHlistOther(allLists);
                }
            }
            TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT);

            UtilsUI.appendTable(allLists, tableLayout, layoutParams, context);

        } else if (line.contains("Fosforanowanie") || line.contains("KTL")) {
            appPreferences = new AppPreferences(context);

            List<String> newScan = new ArrayList<>();
            newScan.add(barcodeData);
            if (quantity.isEmpty()) {
                return;
            } else {
                newScan.add(String.format("%s szt.", quantity));
            }
            newScan.add(time);
            List<List<String>> allLists;

            allLists = appPreferences.getWHlistOther();

            if (!appPreferences.containsWHList(newScan, allLists)){
                allLists.add(newScan);
                appPreferences.setWHlistOther(allLists);
            }

            TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT);

            UtilsUI.appendTable(allLists, tableLayout, layoutParams, context);
        } else {
            appPreferences = new AppPreferences(context);

            List<String> newScan = new ArrayList<>();
            newScan.add(barcodeData);
            newScan.add(barcodeData2);
            newScan.add(time);
            List<List<String>> allLists;

            allLists = appPreferences.getWHlistOther();

            if (!appPreferences.containsWHList(newScan, allLists) && !Objects.equals(codeData3, "Delete")){
                allLists.add(newScan);
                appPreferences.setWHlistOther(allLists);
            }

            TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT);

            UtilsUI.appendTable(allLists, tableLayout, layoutParams, context);
        }
    }
    private TextView createTextView(String text) {
        TextView textView = new TextView(context);
        textView.setText(text);
        textView.setTextSize(20);
        textView.setGravity(Gravity.CENTER);
        textView.setPadding(8, 8, 8, 24);
        return textView;
    }
    private void showAlertDialog(String title, String message, String colorName, String packaging) {
            MaterialAlertDialogBuilder builder;

            if (colorName != null && !colorName.isEmpty()) {
                // Determine the style based on the colorName
                int styleResId;
                switch (colorName.toLowerCase()) {
                    case "red":
                        styleResId = R.style.AlertDialogStyleRed;
                        break;
                    case "green":
                        styleResId = R.style.AlertDialogStyleGreen;
                        break;
                    case "yellow":
                        styleResId = R.style.AlertDialogStyleYellow;
                        break;
                    default:
                        // Default style
                        styleResId = R.style.AlertDialogStyleDefault;
                }

                // Create the AlertDialog.Builder with the specified style
                builder = new MaterialAlertDialogBuilder(context, styleResId);
            } else {
                // Default style if no colorName is provided
                builder = new MaterialAlertDialogBuilder(context, R.style.AlertDialogStyleDefault);

            }
            builder.setTitle(title)
                    .setMessage(message)
                    .setPositiveButton("OK", (dialog, which) -> {
                        // You can perform additional actions here if needed
                    })
                    .show();

            if (Objects.equals(line, "Kontrola jakości (KTL)") && !packaging.contains("Nie przypisano opakowania") && !packaging.isEmpty()) {
                Log.d("Pack", packaging);
                MaterialAlertDialogBuilder builderPack = new MaterialAlertDialogBuilder(context);
                View customLayout = LayoutInflater.from(context).inflate(R.layout.mm_packaging_alert, null);

                TextView dialogTitle = customLayout.findViewById(R.id.dialog_title);

                LinearLayout linearLayout = customLayout.findViewById(R.id.scroll_linear);

                Pattern pattern = Pattern.compile("[bB][0-9]+");
                Matcher matcher = pattern.matcher(packaging);

                Map<String, Integer> resourceMap = new HashMap<>();
                resourceMap.put("b02", R.drawable.b02);
                resourceMap.put("b08", R.drawable.b08);
                resourceMap.put("b22", R.drawable.b22);
                resourceMap.put("b24", R.drawable.b24);
                resourceMap.put("b26", R.drawable.b26);
                resourceMap.put("b33", R.drawable.b33);
                resourceMap.put("b40", R.drawable.b40);
                resourceMap.put("b55", R.drawable.b55);
                resourceMap.put("b71", R.drawable.b71);
                resourceMap.put("b75", R.drawable.b75);
                resourceMap.put("b79", R.drawable.b79);
                resourceMap.put("b80", R.drawable.b80);
                resourceMap.put("b90", R.drawable.b90);
                resourceMap.put("b101", R.drawable.b101);

                int matchCount = 0;
                while (matcher.find()) {
                    matchCount++;
                }
                matcher = pattern.matcher(packaging);

                if (matcher.find()) {
                    do {
                        String match = matcher.group().toLowerCase();

                        Integer resourceId = resourceMap.get(match); // Get the corresponding resource ID from the mapping
                        if (resourceId != null) {
                            if (matchCount > 1) {
                                TextView titleTextView = new TextView(context);
                                titleTextView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                                titleTextView.setText(match.toUpperCase());
                                titleTextView.setGravity(Gravity.CENTER);
                                titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                                linearLayout.addView(titleTextView);
                            }
                            ImageView imageView = new ImageView(context);
                            imageView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                            imageView.setImageResource(resourceId);
                            linearLayout.addView(imageView);
                        }

                    } while (matcher.find());
                }

                dialogTitle.setText(packaging);


                builderPack.setView(customLayout)
                           .setCancelable(false)
                           .setPositiveButton("OK", (dialog, which) -> {
                                // You can perform additional actions here if needed
                            }).show();
            }
    }
}
